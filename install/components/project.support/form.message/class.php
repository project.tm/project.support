<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Support\Form,
    Project\Support\Message,
    Project\Support\Theme,
    Project\Support\Config;

CBitrixComponent::includeComponentClass("project.support:form.support");

class PortalFormMessage extends PortalFormSupport {

    public function executeComponent() {
        if (cUser::IsAuthorized()) {
            return parent::executeComponent();
        }
    }

    protected function filterForm(&$arData) {
        $request = Application::getInstance()->getContext()->getRequest();
        if (empty($request->getPost('MESSAGE'))) {
            $this->arResult['ERROR']['MESSAGE'] = 'Поле «Cообщение» не заполнено';
        } else {
            $arData['MESSAGE'] = $request->getPost('MESSAGE');
        }
    }

    protected function sendForm($arData) {
        $arData['SELLER_ID'] =  $this->arParams['SELLER_ID'];
        $arData['GAME_ID'] = $this->arParams['GAME']['ID'];
        $arData['THEME'] = $this->arParams['GAME']['THEME'];

        return Message::add($this->arParams['TYPE'], $arData);
    }

}
