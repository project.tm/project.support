<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "PAGEN" => $arResult["PAGEN"],
        "SELECT" => '.message-list-contaner .remove-icon',
        "CONTANER" => '.message-list-contaner',
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        jsPortalMessageList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>