(function (window) {

    if (window.jsPortalMessageList) {
        return;
    }

    window.jsPortalMessageList = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            select: arParams.SELECT,
            contaner: arParams.CONTANER
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            PAGEN_1: arParams.PAGEN
        };
        $(document).on('click', self.config.select, function () {
            self.param.THEME_ID = $(this).data('id');
            self.loadAjax();
        });

        self.loadAjax = function (func) {
            $.get(self.config.ajax, self.param, function (data) {
                $(self.config.contaner).html(data.content);
                if(func) {
                    func();
                }
            }, 'json');
        };
    };
})(window);