<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var array $arParams
 * @var array $arResult
 */
if (empty($arResult["TOPIC"]))
    return 0;

// ************************* Input params***************************************************************
$arParams["SHOW_NAV"] = (is_array($arParams["SHOW_NAV"]) ? $arParams["SHOW_NAV"] : array());
$arParams["SHOW_COLUMNS"] = (is_array($arParams["SHOW_COLUMNS"]) ? $arParams["SHOW_COLUMNS"] : array());
$arParams["SHOW_SORTING"] = ($arParams["SHOW_SORTING"] == "Y" ? "Y" : "N");
$arParams["SEPARATE"] = (empty($arParams["SEPARATE"]) ? GetMessage("FTP_IN_FORUM") : $arParams["SEPARATE"]);
// *************************/Input params***************************************************************
?>
<? if($arResult["TOPIC"]) { ?>
<script>
    var portalForumMessageList = new Array();
</script>
<? foreach ($arResult["TOPIC"] as $arTopic) { ?>
    <div class="messages-block toggle-js messages-block-<?= $arTopic['ID'] ?>">
        <?
        $APPLICATION->IncludeComponent("project.support:forum.topic.mesage", $arTopic['FORUM_ID']==5 ? 'points' : '.default', array(
            'THEME_ID' => $arTopic['ID']
        ));
        ?>
    </div>
<? } ?>
<? if (in_array("BOTTOM", $arParams["SHOW_NAV"])) { ?>
    <?= $arResult["NAV_STRING"] ?>
<? } ?>
<? } else { ?>
    <div class="personal-list-empty">Нет сообщений</div>
<? } ?>
