<?

define("FAVORIT_AJAX", true);
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$arResult['isNext'] = $APPLICATION->IncludeComponent("project.support:forum.topic.mesage", $_REQUEST["TEMPLATE_NAME"], Array(
    "IS_AJAX" => 'Y',
    "THEME_ID" => $_REQUEST["THEME_ID"],
    "PAGE" => $_REQUEST["PAGE"] ?: 1,
    "IS_UPDATE" => $_REQUEST["IS_UPDATE"] ?: 0
        ));
$arResult['content'] = ob_get_clean();
echo json_encode($arResult);
