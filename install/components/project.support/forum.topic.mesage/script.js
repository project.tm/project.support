(function (window) {

    if (window.jsPortalForumMessageList) {
        return;
    }

    window.jsPortalForumMessageList = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            more: arParams.CONTANER + ' ' + arParams.MORE,
            comment: arParams.CONTANER + ' ' + arParams.COMMENT,
            contaner: arParams.CONTANER
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            THEME_ID: arParams.THEME_ID,
            PAGE: 1
        };

        $(document).on('click', self.config.more, function () {
            self.param.PAGE++;
            self.param.IS_UPDATE = 0;
            self.loadAjax(this);
        });

        self.update = function (func) {
            self.param.IS_UPDATE = 1;
            self.loadAjax(false, func);
        };

        self.loadAjax = function (t, func) {
            if (t) {
                t = $(t);
                t.data('html', t.html()).text("Загрузка... ");
            }
            $.get(self.config.ajax, self.param, function (data) {
                if (self.param.IS_UPDATE) {
                    $(self.config.contaner).html(data.content);
                } else {
                    $(self.config.comment).append(data.content);
                }
                if (data.isNext) {
                    $(self.config.more).removeClass('hidden');
                } else {
                    $(self.config.more).addClass('hidden');
                }
                if (t) {
                    t.removeClass('active').html(t.data('html')).html(t.data('html'));
                }
                if(func) {
                    func();
                }
            }, 'json').fail(function () {
                if (t) {
                    t.text("Не удалось загрузить страницу, попробуйте позже")
                            .delay(1500)
                            .queue(function (n) {
                                $(this).html(t.data('html'));
                                n();
                            });
                }
            });
        };
    };
})(window);