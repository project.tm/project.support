<div class="base-style block-kommisar <? if (!empty($arResult['IS_SEND'])) { ?>success-img<? } ?>" <? if ($arResult['IS_AJAX']) { ?>style="display: block;"<? } ?>>
    <form id="form_com" class="<?= $arParams['FORM_SELECT'] ?>">
        <input type="hidden" id="form-send" name="form-send" value="<?= $arResult['FORM_SEND'] ?>">
        <?= bitrix_sessid_post() ?>
        <div class="head_kommis">
            <div class="left_underline"></div>
            <div class="center_icon"></div>
            <div class="right_underline"></div>
        </div>
        <? if (!empty($arResult['ERROR'])) { ?>
            <div class="u-error">
                <? ShowMessage(implode('<br>', $arResult['ERROR'])) ?>
            </div>
        <? } ?>
        <div class="u-question">
            <div class="text">изложите вопрос комиссару:</div>
            <div class="wrap_textarea">
                <textarea name="MESSAGE"><?= htmlspecialcharsBx($arResult['DATA']['MESSAGE'] ?: '') ?></textarea>
            </div>
        </div>
        <div class="u-mail">
            <? if ($arResult['IS_USER']) { ?>
                <div class="text">
                    <div>* ответ придет в личном сообщении</div>
                </div>
            <? } else { ?>
                <div class="text">куда отправить ответ
                    <div>* если вы авторизованы, поле заполнять не обязательно: ответ придет в личное сообщение</div>
                </div>
                <input type="email" name="EMAIL" placeholder="E-MAIL" value="<?= htmlspecialcharsBx($arResult['DATA']['EMAIL'] ?: '') ?>">
            <? } ?>
        </div>
        <button class="send_btn_komm">отправить КОМИССАРУ</button>
    </form>
    <a class="close-popup"></a>
</div>