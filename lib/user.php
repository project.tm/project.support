<?php

namespace Project\Support;

use CUser;

class User {
    
    public function isModerator() {
        static $is = null;
        if (is_null($is)) {
            global $USER;
            foreach (CUser::GetUserGroup($USER->GetID()) as $id) {
                if (in_array($id, Config::MODERATOR)) {
                    $is = true;
                    break;
                }
            }
        }
        return $is;
    }

}
