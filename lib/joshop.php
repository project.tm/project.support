<?php

namespace Project\Support;

use Project\Core\Utility,
    Igromafia\Game,
    CIBlockElement;

class JoShop {

    static public function getById($gameId) {
        return Utility::useCache(array(__CLASS__, __FUNCTION__, $gameId), function() use($gameId) {
                    $arSelect = Array("ID", "NAME", 'PROPERTY_SELLER');
                    $arFilter = Array("IBLOCK_ID" => Game\Config::DZHO_IBLOCK, "ID" => $gameId);
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    while ($arItem = $res->GetNext()) {
                        return array(
                            'ID' => $arItem['ID'],
                            'SELLER_ID' => $arItem['PROPERTY_SELLER_VALUE'],
                            'THEME' => [
                                'USER' => 'Покупка игры',
                                'SELLER' => 'Продажа игры',
                                'FORUM' => 'Продажа игры: «' . $arItem['NAME'] . '»'
                            ]
                        );
                    }
                    return false;
                });
    }

}
