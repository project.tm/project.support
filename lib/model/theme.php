<?php

namespace Project\Support\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ThemeTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'd_project_support_theme';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'ACTIVE' => array(
                'data_type' => 'integer',
                'default_value' => 1
            ),
            'BANNED' => array(
                'data_type' => 'integer',
                'default_value' => 0
            ),
            'USER' => array(
                'data_type' => 'integer',
                'default_value' => static::getUserId()
            ),
            'INTERLOCUTOR' => array(
                'data_type' => 'string'
            ),
            'FORUM_ID' => array(
                'data_type' => 'integer',
            ),
            'THEME_ID' => array(
                'data_type' => 'integer',
            ),
            'THEME_START' => array(
                'data_type' => 'integer',
                'default_value' => 0
            ),
            'THEME' => array(
                'data_type' => 'integer'
            ),
            'GAME_ID' => array(
                'data_type' => 'string',
                'default_value' => 0
            ),
            'NEW' => array(
                'data_type' => 'integer',
                'default_value' => 0
            ),
        );
    }

    public static function getUserId() {
        global $USER;
        return $USER ? $USER->GetID() : null;
    }

}
